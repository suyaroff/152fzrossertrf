function sendOrder() {
  var order = $('#order input').serializeArray();
  if (order.length < 6) {
    alert('условия передачи информации');
    return false;
  }
  if (order[1].value == '') {
    alert('Введите Фамилия, имя и отчество');
    return false;
  }
  if (order[2].value == '') {
    alert('Введите номер телефона');
    return false;
  }
  if (order[3].value == '') {
    alert('Введите email');
    return false;
  }

  var inn = window.location.href.match(/utm_campaign.*-(\d*)/);
  if(inn){
    order.push({name:'inn', value: inn[1]});
  } else {
    order.push({name:'inn', value: ''});
  }

  $.post('/send.php', order, function (r) {
    if (r == 'success') {
      alert('Заявка оптравлена');
      $('input[name="name"]').val('');
      $('input[name="phone"]').val('');
      $('input[name="email"]').val('');
    }
    if (r == 'error') {
      alert('Ошибка, пиьсмо не отправлено');
    }
  });

}

$(document).ready(function () {
  $('#section_8 .reviews').slick(
    {
      autoplay: true,
      prevArrow: '#section_8 .ar-r',
      nextArrow: '#section_8 .ar-l',
      dots: true
    }
  );
  $('button').click(function () {
    $.scrollTo('#section_10', 600);
  });
  $('div.info').click(function () {
    $.scrollTo('#section_6', 600);
  });
  $('span.ask').click(function () {
    $.scrollTo('#section_10', 600);
  });
  $('a.open-window').click(function () {
    $.scrollTo('#section_10', 600);
  });
});
